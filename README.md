# Салон Красоты



# Описание предметной области
Система «Салон красоты» предназначена для просмотра оказанных или проводимых в будущем услуг. Также система предназначена для внесения новой информации об услугах, клиентах, работниках, просмотр и изменение данных, охватывает различные аспекты, связанные с организацией работы и повседневной деятельностью сотрудников салона красоты. Основная цель такого приложения - упростить и автоматизировать процессы, связанные с работой персонала салона.
Всю информацию можно найти в техническом задании.

# ERD-диаграмма
<p align="center"><img src="https://sun9-75.userapi.com/impg/NhJwsRDv8ueOUA9r48Uqu0Tam7LNOpcXn4XKWA/G9_r_0gXWow.jpg?size=2160x2160&quality=96&sign=3e650fa998945ee4b0d0914b42db018b&type=album" /></p>

# Use-case диаграмма
<p align="center"><img src="https://sun9-34.userapi.com/impg/--n9LQlyzRJqruqShimXg5D8_fpNUmR8E1QnQg/SHG4Tobc-OU.jpg?size=901x604&quality=96&sign=dd74edf268b9031b158c17ebe232e9d7&type=album" /></p>

# Ссылка на Figma
https://www.figma.com/file/NdWaC2UpqRw4lblnls1qUF?type=design

# Диаграмма последовательностей
<p align="center"><img src="https://avatars.mds.yandex.net/get-images-cbir/1544561/gsf0dx1QRkXiWLkQZ10PAg6724/ocr" /></p>
<p align="center"><img src="https://avatars.mds.yandex.net/get-images-cbir/4480543/kooIpRSjEQCVOwMPgehKEQ6750/ocr" /></p>

# Диаграмма классов
<p align="center"><img src="https://avatars.mds.yandex.net/get-images-cbir/3540569/gszzlazdbxoKYuMXBdfr7Q6639/ocr" /></p>

# Диаграмма активности
<p align="center"><img src="https://avatars.mds.yandex.net/get-images-cbir/1365097/173CcLLOdsz1I73WLRmg4w6742/ocr" /></p>
