﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BeautySalon.DB;
using static BeautySalon.ClassHelper.EFClass;

namespace BeautySalon.Windows
{
    /// <summary>
    /// Логика взаимодействия для AddEditProductWindow.xaml
    /// </summary>
    public partial class AddEditProductWindow : Window
    {
        private bool isChange = false;
        private Product editProduct;
        public AddEditProductWindow()
        {
            InitializeComponent();

            cmbCategory.ItemsSource = Context.CategoryProduct.ToList();
            cmbCategory.SelectedIndex = 0;
            cmbCategory.DisplayMemberPath = "Title";
        }

        public AddEditProductWindow(Product product)
        {
            InitializeComponent();

            cmbCategory.ItemsSource = Context.CategoryProduct.ToList();
            cmbCategory.SelectedIndex = 0;
            cmbCategory.DisplayMemberPath = "Title";

            tbTitle.Text = product.Title.ToString();
            tbCost.Text = product.Cost.ToString();
            cmbCategory.SelectedItem = Context.CategoryProduct.Where(i => i.IdCategoryProduct == product.IdCategoryProduct).FirstOrDefault();
            tbQuantityInStock.Text = product.QuantityInStock.ToString();

            isChange = true;
            editProduct = product;
        }

        private void btnAddEdit_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbTitle.Text) ||
                string.IsNullOrWhiteSpace(tbCost.Text) ||
                string.IsNullOrWhiteSpace(tbQuantityInStock.Text))
            {
                MessageBox.Show("Не все поля заполнены!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            if (isChange)
            {
                editProduct.Title = tbTitle.Text;
                editProduct.Cost = Convert.ToDecimal(tbCost.Text);
                editProduct.IdCategoryProduct = (cmbCategory.SelectedItem as CategoryProduct).IdCategoryProduct;
                editProduct.QuantityInStock = Convert.ToInt32(tbQuantityInStock.Text);

                Context.SaveChanges();
                MessageBox.Show("Товар успешно обновлен!", "Успех!", MessageBoxButton.OK, MessageBoxImage.Information);
                this.Close();
            }
            else
            {
                Product product = new Product();
                product.Title = tbTitle.Text;
                product.Cost = Convert.ToDecimal(tbCost.Text);
                product.IdCategoryProduct = (cmbCategory.SelectedItem as CategoryProduct).IdCategoryProduct;
                product.QuantityInStock = Convert.ToInt32(tbQuantityInStock.Text);

                Context.Product.Add(product);
                Context.SaveChanges();
                MessageBox.Show("Товар успешно добавлен", "Успех!", MessageBoxButton.OK, MessageBoxImage.Information);
                this.Close();
            }
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            ListProductWindow listProductWindow = new ListProductWindow();
            listProductWindow.Show();
            this.Close();
        }
    }
}
