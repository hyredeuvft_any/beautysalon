﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BeautySalon.Windows;
using BeautySalon.ClassHelper;

namespace BeautySalon.Windows
{
    /// <summary>
    /// Логика взаимодействия для HomeWindow.xaml
    /// </summary>
    public partial class HomeWindow : Window
    {
        public HomeWindow()
        {
            InitializeComponent();
            //tblWelcome.Text = "Добро пожаловать, " + EmployeeDataClass.Employee.FirstName;
            //tblData.Text = EmployeeDataClass.Employee.LastName + EmployeeDataClass.Employee.FirstName + EmployeeDataClass.Employee.Patronymic 
            //    + EmployeeDataClass.Employee.Birthday + EmployeeDataClass.Employee.Phone + EmployeeDataClass.Employee.Email + EmployeeDataClass.Employee.Gender.Title;
        }

        private void btnListService_Click(object sender, RoutedEventArgs e)
        {
            ListServiceWindow listServiceWindow = new ListServiceWindow();
            listServiceWindow.Show();
            this.Close();
        }

        private void btnListClient_Click(object sender, RoutedEventArgs e)
        {
            ListClientWindow listClientWindow = new ListClientWindow();
            listClientWindow.Show();
            this.Close();
        }

        private void btnListEmployee_Click(object sender, RoutedEventArgs e)
        {
            ListEmployeeWindow listEmployeeWindow = new ListEmployeeWindow();
            listEmployeeWindow.Show();
            this.Close();
        }

        private void btnListRecord_Click(object sender, RoutedEventArgs e)
        {
            ListRecordWindow listRecordWindow = new ListRecordWindow();
            listRecordWindow.Show();
            this.Close();
        }

        private void btnListProduct_Click(object sender, RoutedEventArgs e)
        {
            ListProductWindow listProductWindow = new ListProductWindow();
            listProductWindow.Show();
            this.Close();
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            var DialogResult = MessageBox.Show("Вы уверены, что хотите выйти?", "Внимание!", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (DialogResult == MessageBoxResult.Yes)
            {
                AuthorizationWindow authWindow = new AuthorizationWindow();
                authWindow.Show();
                this.Close();
            }
            
        }

    }
}
